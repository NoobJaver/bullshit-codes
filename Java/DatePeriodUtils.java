public DatePeriodUtils{
    /**
    * 一天24小时，按照半小时划分，共48段。数据库设置48个字段，字段命名规则 period1、period2、period3 ......
    * 取值，修改，判断比较，都有类似方法，只展示一种，其它省略
    */

    /**
     * 给定时间段和状态设置对应时段的period状态值
     *
     * @param reservationPeriodVO
     * @param startTime
     * @param endTime
     * @return
     */
    public static ReservationPeriodVO getPeriodValueForInvalid(ReservationPeriodVO reservationPeriodVO, Integer startTime, Integer endTime) {
        //step:拿到vo,修改开始到结束时间的period值为想要修改的值value
        if (ObjectUtils.isEmpty(startTime) || ObjectUtils.isEmpty(endTime)) {
            return reservationPeriodVO;
        }
        if (startTime > endTime) {
            return reservationPeriodVO;
        }
        for (int i = startTime; i < endTime; i++) {
            if (i == 1 && reservationPeriodVO.getPeriod1() == null) {
                reservationPeriodVO.setPeriod1(2);
            } else if (i == 2 && reservationPeriodVO.getPeriod2() == null) {
                reservationPeriodVO.setPeriod2(2);
            } else if (i == 3 && reservationPeriodVO.getPeriod3() == null) {
                reservationPeriodVO.setPeriod3(2);
            } else if (i == 4 && reservationPeriodVO.getPeriod4() == null) {
                reservationPeriodVO.setPeriod4(2);
            } else if (i == 5 && reservationPeriodVO.getPeriod5() == null) {
                reservationPeriodVO.setPeriod5(2);
            } else if (i == 6 && reservationPeriodVO.getPeriod6() == null) {
                reservationPeriodVO.setPeriod6(2);
            } else if (i == 7 && reservationPeriodVO.getPeriod7() == null) {
                reservationPeriodVO.setPeriod7(2);
            } else if (i == 8 && reservationPeriodVO.getPeriod8() == null) {
                reservationPeriodVO.setPeriod8(2);
            } else if (i == 9 && reservationPeriodVO.getPeriod9() == null) {
                reservationPeriodVO.setPeriod9(2);
            } else if (i == 10 && reservationPeriodVO.getPeriod10() == null) {
                reservationPeriodVO.setPeriod10(2);
            } else if (i == 11 && reservationPeriodVO.getPeriod11() == null) {
                reservationPeriodVO.setPeriod11(2);
            } else if (i == 12 && reservationPeriodVO.getPeriod12() == null) {
                reservationPeriodVO.setPeriod12(2);
            } else if (i == 13 && reservationPeriodVO.getPeriod13() == null) {
                reservationPeriodVO.setPeriod13(2);
            } else if (i == 14 && reservationPeriodVO.getPeriod14() == null) {
                reservationPeriodVO.setPeriod14(2);
            } else if (i == 15 && reservationPeriodVO.getPeriod15() == null) {
                reservationPeriodVO.setPeriod15(2);
            } else if (i == 16 && reservationPeriodVO.getPeriod16() == null) {
                reservationPeriodVO.setPeriod16(2);
            } else if (i == 17 && reservationPeriodVO.getPeriod17() == null) {
                reservationPeriodVO.setPeriod17(2);
            } else if (i == 18 && reservationPeriodVO.getPeriod18() == null) {
                reservationPeriodVO.setPeriod18(2);
            } else if (i == 19 && reservationPeriodVO.getPeriod19() == null) {
                reservationPeriodVO.setPeriod19(2);
            } else if (i == 20 && reservationPeriodVO.getPeriod20() == null) {
                reservationPeriodVO.setPeriod20(2);
            } else if (i == 21 && reservationPeriodVO.getPeriod21() == null) {
                reservationPeriodVO.setPeriod21(2);
            } else if (i == 22 && reservationPeriodVO.getPeriod22() == null) {
                reservationPeriodVO.setPeriod22(2);
            } else if (i == 23 && reservationPeriodVO.getPeriod23() == null) {
                reservationPeriodVO.setPeriod23(2);
            } else if (i == 24 && reservationPeriodVO.getPeriod24() == null) {
                reservationPeriodVO.setPeriod24(2);
            } else if (i == 25 && reservationPeriodVO.getPeriod25() == null) {
                reservationPeriodVO.setPeriod25(2);
            } else if (i == 26 && reservationPeriodVO.getPeriod26() == null) {
                reservationPeriodVO.setPeriod26(2);
            } else if (i == 27 && reservationPeriodVO.getPeriod27() == null) {
                reservationPeriodVO.setPeriod27(2);
            } else if (i == 28 && reservationPeriodVO.getPeriod28() == null) {
                reservationPeriodVO.setPeriod28(2);
            } else if (i == 29 && reservationPeriodVO.getPeriod29() == null) {
                reservationPeriodVO.setPeriod29(2);
            } else if (i == 30 && reservationPeriodVO.getPeriod30() == null) {
                reservationPeriodVO.setPeriod30(2);
            } else if (i == 31 && reservationPeriodVO.getPeriod31() == null) {
                reservationPeriodVO.setPeriod31(2);
            } else if (i == 32 && reservationPeriodVO.getPeriod32() == null) {
                reservationPeriodVO.setPeriod32(2);
            } else if (i == 33 && reservationPeriodVO.getPeriod33() == null) {
                reservationPeriodVO.setPeriod33(2);
            } else if (i == 34 && reservationPeriodVO.getPeriod34() == null) {
                reservationPeriodVO.setPeriod34(2);
            } else if (i == 35 && reservationPeriodVO.getPeriod35() == null) {
                reservationPeriodVO.setPeriod35(2);
            } else if (i == 36 && reservationPeriodVO.getPeriod36() == null) {
                reservationPeriodVO.setPeriod36(2);
            } else if (i == 37 && reservationPeriodVO.getPeriod37() == null) {
                reservationPeriodVO.setPeriod37(2);
            } else if (i == 38 && reservationPeriodVO.getPeriod38() == null) {
                reservationPeriodVO.setPeriod38(2);
            } else if (i == 39 && reservationPeriodVO.getPeriod39() == null) {
                reservationPeriodVO.setPeriod39(2);
            } else if (i == 40 && reservationPeriodVO.getPeriod40() == null) {
                reservationPeriodVO.setPeriod40(2);
            } else if (i == 41 && reservationPeriodVO.getPeriod41() == null) {
                reservationPeriodVO.setPeriod41(2);
            } else if (i == 42 && reservationPeriodVO.getPeriod42() == null) {
                reservationPeriodVO.setPeriod42(2);
            } else if (i == 43 && reservationPeriodVO.getPeriod43() == null) {
                reservationPeriodVO.setPeriod43(2);
            } else if (i == 44 && reservationPeriodVO.getPeriod44() == null) {
                reservationPeriodVO.setPeriod44(2);
            } else if (i == 45 && reservationPeriodVO.getPeriod45() == null) {
                reservationPeriodVO.setPeriod45(2);
            } else if (i == 46 && reservationPeriodVO.getPeriod46() == null) {
                reservationPeriodVO.setPeriod46(2);
            } else if (i == 47 && reservationPeriodVO.getPeriod47() == null) {
                reservationPeriodVO.setPeriod47(2);
            } else if (i == 48 && reservationPeriodVO.getPeriod48() == null) {
                reservationPeriodVO.setPeriod48(2);
            }
        }
        return reservationPeriodVO;
    }


}