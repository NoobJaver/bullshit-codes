// 检测对象是否是空对象(真实案例，项目里面用来判断是否未初始化，如果没有则进行初始化操作)
function is_empty_object(obj) {
  return JSON.stringify(obj) == '{}';
}

// 然后就加了数组
function is_empty_array(arr) {
    return JSON.stringify(arr) == '[]';
}
